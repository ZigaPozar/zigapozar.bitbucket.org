
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {

  var ime;
  var priimek;
  var datumRojstva;
  var date;
  var visina;
  var teza;
  var temperatura;
  var sistolicni;
  var diastolicni;
  var kisik;
  var merilec = "medicinska sestra Marjeta";
  
  if(stPacienta == 1){
    ime = "Janez";
    priimek = "Novak";
    datumRojstva = "1985-02-06";
    date = "2019-04-08T11:40Z";
    visina = "177";
    teza = "80.0";
    temperatura = "36.60";
    sistolicni = "131";
    diastolicni = "86";
    kisik = "95";
  }
  if(stPacienta == 2){
    ime = "Luka";
    priimek = "Dončić";
    datumRojstva = "1999-02-28";
    date = "2019-04-25T13:30Z";
    visina = "203";
    teza = "103.0";
    temperatura = "36.70";
    sistolicni = "116";
    diastolicni = "78";
    kisik = "99";
  }
  if(stPacienta == 3){
    ime = "John";
    priimek = "Smith";
    datumRojstva = "1988-10-15";
    date = "2019-05-03T10:20Z";
    visina = "186";
    teza = "150.0";
    temperatura = "36.73";
    sistolicni = "163";
    diastolicni = "102";
    kisik = "90";
  }

  // TODO: Potrebno implementirati
     $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            var podatki = {
        		    "ctx/language": "en",
        		    "ctx/territory": "SI",
        		    "ctx/time": date,
        		    "vital_signs/height_length/any_event/body_height_length": visina,
        		    "vital_signs/body_weight/any_event/body_weight": teza,
        		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
        		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
        		    "vital_signs/blood_pressure/any_event/systolic": sistolicni,
        		    "vital_signs/blood_pressure/any_event/diastolic": diastolicni,
        		    "vital_signs/indirect_oximetry:0/spo2|numerator": kisik
        		};
            var parametriZahteve = {
      		    ehrId: ehrId,
      		    templateId: 'Vital Signs',
      		    format: 'FLAT',
      		    committer: merilec
      		};
      		$.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            headers: {
              "Authorization": getAuthorization()
            },
            success: function (res) {
              console.log("USPESNO GENERIRANJE");
            },
            error: function(err) {
              console.log("NEUSPESNO GENERIRANJE, ERROR:" + JSON.parse(err.responseText).userMessage);
            }
      		});
            document.getElementById("izbira"+stPacienta).value=ehrId;
            document.getElementById("izbiraPregled"+stPacienta).value=ehrId;
          },
          error: function(err) {
            console.log("Generiranje podatkov ni bilo uspešno! " + err);
          }
        });
      }
		});
		
}

function generiranjePodatkov(){
  
  generirajPodatke(1);
  generirajPodatke(2);
  generirajPodatke(3);
}

//Za zapolnitev polj s podatki bolnikov izbranih iz padajočih seznamov
$(document).ready(function() {
  
  //Za zapolnitev polj "Kreiranje EHR zapisa" s podatki bolnikov izbranih iz padajočega seznama
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  //Za zapolnitev polj "Preberi EHR zapis obstoječega bolnika" s podatki bolnikov izbranih iz padajočega seznama
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
	
	//Za zapolnitev polj "Vnos meritve vitalnih znakov bolnika" s podatki bolnikov izbranih iz padajočega seznama
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  //Za branje vitalnih znakov bolnika izbranega iz padajočega seznama
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
});



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

//Dodajanje podatkov o bolniku
function kreirajEHRzaBolnika() {
  //Osnovne informacije o bolniku:
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 || priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<div class='alert alert-danger' role='alert'>Prosim vnesite vse zahtevane podatke!</div>");
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<div class='alert alert-success' role='alert'>Uspešno kreiran EHR '" +
                        ehrId + "'.</div>");
                      $("#preberiEHRid").val(ehrId);
                      $("#dodajVitalnoEHR").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<div class='alert alert-danger' role='alert'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!</div>");
          }
        });
      }
		});
	}
}

function dodajMeritveKrvnegaTlaka() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = "36.60";
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = "" + (Math.floor(Math.random(10) * 10) + 90) + "";
	var merilec = "Dr. Nebodigatreba";

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<div class='alert alert-danger' role='alert'>Prosim vnesite vse zahtevane podatke!</div>");
	} else {
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("<div class='alert alert-success' role='alert'>Meritve so bile uspešno dodane!</div>");
                      $("#preberiEHRid").val(ehrId);
      },
      error: function(err) {
        $("#kreirajSporocilo").html("<div class='alert alert-danger' role='alert'>Napaka '" +
                      JSON.parse(err.responseText).userMessage + "'!</div>");
      }
		});
	}
}

function preberiMeritveKrvnegaTlaka() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	
	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<div class='alert alert-danger' role='alert'>Prosim vnesite zahtevan podatek!</div>");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
	    	success: function (data) {
  			var party = data.party;
  			$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Prikaz meritev izmerjenega krvnega tlaka za bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>:</span><br/><br/>");
  				$.ajax({
    			   url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
    			   type: 'GET',
    			   headers: {
              "Authorization": getAuthorization()
            },
    			   success: function (res) {
    			    if (res.length > 0) {
    				    var results = "<table class='table table-striped " +
                  "table-hover'><tr><th>Datum in ura</th>" +
                  "<th class='text-center'>Sistolični krvni tlak</th>" + 
                  "<th class='text-right'>Diastolični krvni tlak</th></tr>";
  				      for (var i in res) {
    		          results += "<tr><td>" + res[i].time +
                    "</td><td class='text-center'>" + res[i].systolic + "" + res[i].unit + 
                    "</td><td class='text-right'>" + res[i].diastolic + "" + res[i].unit + "</tr>";
  				      }
  				      results += "</table>";
  				      $("#rezultatMeritveVitalnihZnakov").append(results);
  				      
  				      //Preverimo, če je krvni tlak normalen ali ne:
  				      if(res[0].systolic < 120 && res[0].diastolic < 80){
  				        $("#ocenitevStanjaKrvnegaTlaka").html("<span>Bolnikov krvni tlak je <b style='color:#66ff33'>NORMALEN.</b>" + 
  				        "<a data-toggle='collapse' href='#collapseExample' aria-expanded='false' aria-controls='collapseExample'>" + 
  				        "<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></a><br>" + 
  				        "<div class='collapse' id='collapseExample'><div class='panel panel-default'><div class='panel-body'>" + 
                  "'Normalni' krvni tlak: sistolični tlak je nižji od 120, diastolični tlak pa nižji od 80 mmHg.</div></div></div>" +
                  "<span>Zaradi normalnega krvnega tlaka, bolniku priporočamo, da nadaljuje z svojim življenskim slogom, saj je ta optimalen.</span><br>" +
                  "<span>Število bolnikov, ki se zdravjo za povišanim krvnim tlakom:</span>");
                  
                  //Pridobivanje podatkov s spletne strani:
  				        pridobivanjePodatkov();
  				      }
  				      else if(res[0].systolic >= 120 && res[0].systolic <= 139 && res[0].diastolic >= 80 && res[0].diastolic <= 89) {
  				        $("#ocenitevStanjaKrvnegaTlaka").html("<span>Bolnik ima <b style='color:#ffff00'>PREDHIPERTENZIJO!</b>" + 
  				        "<a data-toggle='collapse' href='#collapseExample' aria-expanded='false' aria-controls='collapseExample'>" + 
  				        "<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></a><br>" + 
  				        "<div class='collapse' id='collapseExample'><div class='panel panel-default'><div class='panel-body'>" + 
                  "'Predhipertenzija': sistolični krvni tlak znaša od 120 do 139, diastolični krvni tlak pa od 80 do 89 mmHg.</div></div></div>" +
                  "<span>Zaradi rahlo povišanega krvnega tlaka, bolniku priporočamo slednje: jejte zdravo, ohranjajte zdravo telesno težo," +
                  "bodite fizično aktivni, omejite uživanje alkohola, ne kadite.</span><br>"+ 
                  "<span>Število bolnikov, ki se zdravjo za povišanim krvnim tlakom:</span>");
                  
                  //Pridobivanje podatkov s spletne strani:
  				        pridobivanjePodatkov();
  				      }
  				      else if(res[0].systolic >= 140 && res[0].systolic <= 159 && res[0].diastolic >= 90 && res[0].diastolic <= 99){
  				        $("#ocenitevStanjaKrvnegaTlaka").html("<span>Bolnik ima <b style='color:#ff9900'>HIPERTENZIJO 1. STOPNJE!</b>" + 
  				        "<a data-toggle='collapse' href='#collapseExample' aria-expanded='false' aria-controls='collapseExample'>" + 
  				        "<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></a><br>" + 
  				        "<div class='collapse' id='collapseExample'><div class='panel panel-default'><div class='panel-body'>" + 
                  "Hipertenzija 1. stopnje pomeni, da sistolični tlak znaša 140–159 mmHg, diastolični tlak pa 90–99 mmHg.</div></div></div>"+
                  "<span>Zaradi povišanega krvnega tlaka, bolniku priporočamo obisk pri katerem od specialistov.</span><br>" +
                  "<span>Število bolnikov, ki se zdravjo za povišanim krvnim tlakom:</span>");
                  
                  //Pridobivanje podatkov s spletne strani:
  				        pridobivanjePodatkov();
  				      }
  				      else if(res[0].systolic >= 160 && res[0].diastolic >= 100){
  				        $("#ocenitevStanjaKrvnegaTlaka").html("<span>Bolnik ima <b style='color:red'>HIPERTENZIJO 2. STOPNJE!</b>" + 
  				        "<a data-toggle='collapse' href='#collapseExample' aria-expanded='false' aria-controls='collapseExample'>" + 
  				        "<span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span></span></a><br>" + 
  				        "<div class='collapse' id='collapseExample'><div class='panel panel-default'><div class='panel-body'>" + 
                  "Hipertenzija 2. stopnje pomeni, da sistolični tlak znaša 160 mmHg ali več, diastolični tlak pa 100 mmHg ali več.</div></div></div>" +
                  "<span>Zaradi zelo povišanega krvnega tlaka, bolniku zelo priporočamo obisk pri katerem od specialistov.</span><br>" +
                  "<span>Število bolnikov, ki se zdravjo za povišanim krvnim tlakom:</span>");
                  
                  //Pridobivanje podatkov s spletne strani:
  				        pridobivanjePodatkov();
  				      }
  				      
  				      var podatkiSis = new Array();
  				      var podatkiDia = new Array();
  				      
  				      for(var i in res){
  				        podatkiSis[i] = {label:res[i].time, y:res[i].systolic};
  				        podatkiDia[i] = {label:res[i].time, y:res[i].diastolic};
  				      }
  				      console.log(podatkiSis, podatkiDia);
  				      //Kreiranje grafa:
  				      graf(podatkiSis, podatkiDia);
  				      
  				      //Pridobivanje podatkov s spletne strani:
  				      pridobivanjePodatkov();
    
    			    } else {
    			    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<div class='alert alert-danger' role='alert'>" +
                  "Ni podatkov!</div>");
    			    }
    			   },
    			   error: function() {
    			    $("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<div class='alert alert-danger' role='alert'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!</div>");
    			   }
  				});
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<div class='alert alert-danger' role='alert'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!</div>");
	    	}
		});
	}
}

//Tukaj preberemo podatke od izbranega bolnika
function preberiEHRodBolnika() {
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<div class='alert alert-danger' role='alert'>Prosim vnesite zahtevan podatek!</div>");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<div class='alert alert-success' role='alert'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth + "'.</div>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<div class='alert alert-danger' role='alert'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!</div>");
  		}
		});
	}
}

function graf(sistolicni, diastolicni){
  var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	title:{
		text: "Meritve krvnega tlaka"
	},	
	axisY: {
		title: "Sistolični tlak mm[Hg]",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	axisY2: {
		title: "Diastolični tlak mm[Hg]",
		titleFontColor: "#C0504E",
		lineColor: "#C0504E",
		labelFontColor: "#C0504E",
		tickColor: "#C0504E"
	},	
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Sistolični krvni tlak (mm[Hg])",
		legendText: "Sistolični krvni tlak",
		showInLegend: true, 
		dataPoints: sistolicni
	},
	{
		type: "column",	
		name: "Diastolični krvni tlak (mm[Hg])",
		legendText: "Diastolični krvni tlak",
		axisYType: "secondary",
		showInLegend: true,
		dataPoints:diastolicni
	}]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}
}

function pridobivanjePodatkov(){
  
  $.ajax({
      url: "https://api.fda.gov/drug/event.json?api_key=v2ZQ2PaztHj3aKYh1nb3CgKI3zcBlgn33RNxH99b&search=receivedate:[20040101+TO+20190518]&count=patient.drug.drugindication.exact",
      type: "GET",
      success: function (data) {
        console.log(data);
        //console.log(data.results[3].count);
        document.getElementById("steviloZdravljenih").innerHTML = data.results[3].count;
      },
      error: function(err) {
        alert("Prišlo je do napake pri pošiljanju sporočila. Prosimo poskusite znova!");
      }
    });
}